package com.devops.calculator;

import org.junit.jupiter.api.*;

import javax.ws.rs.GET;
import static org.junit.jupiter.api.Assertions.*;


public class CalculatorServiceTest {
    CalculatorService calculator;

    @BeforeEach
    void setupBeforeEach(){
         calculator = new CalculatorService();
        System.out.println("@BeforeEach executes before the execution of each test method");
    }

    @AfterEach
    void tearDownAfterEach(){
        System.out.println("Running @AfterEach");
    }

    @BeforeAll
    static void setupBeforeAll() {
        System.out.println("@BeforeAll executes only once before all test methods execution in the class");
    }

    @AfterAll
    static void tearDownAfterAll() {
        System.out.println("@AfterAll executes only once after all test methods execution in the class");
    }

    @Test
    public void addTest() {

        System.out.println("Running test: addTest");

        assertEquals(6, calculator.Add(2,4).getResult());
        assertNotEquals(6, calculator.Add(5,9).getResult());
    }

    @Test
    public void testNullAndNotNull() {

        CalculatorService calculator = new CalculatorService();

        System.out.println("Running test: testNullAndNotNull");
        String str1 = null;
        String str2 = "luv2code";

        assertNull(calculator.checkNull(str1));
        assertNotNull(calculator.checkNull(str2));

    }

}
